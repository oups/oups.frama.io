       IDENTIFICATION DIVISION.
       PROGRAM-ID. COBOL-IS-FUTURE.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-ACCEPT PIC X(8).
       01 WS-DATE-US.
           05 WS-MM PIC 99.
           05 WS-DD PIC 99.
           05 WS-YYYY PIC 9999.

       01 WS-DATE-WW.
           05 WS-DD PIC 99.
           05 WS-MM PIC 99.
           05 WS-YYYY PIC 9999.

       66 WS-DDMM RENAMES WS-DD OF WS-DATE-WW
           THROUGH WS-MM OF WS-DATE-WW.

       01 WS-DISPLAY-DATE PIC 99/99/9999.
       01 WS-DISPLAY-DATE-ALT REDEFINES WS-DISPLAY-DATE PIC 99.99.9999.

       01 WS-YEAR PIC 9(4).
           88 WS-21ST-CENT VALUE 2000 THROUGH 2099.
           88 WS-20TH-CENT VALUE 1900 THROUGH 1999.

       01 WS-DATE-FORMAT PIC AAAA.
           88 WS-US VALUE "US".
           88 WS-WW VALUE "WW".

       01 WS-AGE PIC B99.

       01  WS-CURRENT-DATE-FIELDS.
           05  WS-CURRENT-DATE.
           10  WS-CURRENT-YEAR    PIC  9(4).
           10  WS-CURRENT-MONTH   PIC  9(2).
           10  WS-CURRENT-DAY     PIC  9(2).
           05  WS-CURRENT-TIME.
           10  WS-CURRENT-HOUR    PIC  9(2).
           10  WS-CURRENT-MINUTE  PIC  9(2).
           10  WS-CURRENT-SECOND  PIC  9(2).
           10  WS-CURRENT-MS      PIC  9(2).
           05  WS-DIFF-FROM-GMT       PIC S9(4).

       PROCEDURE DIVISION.
       PR-START SECTION.
       PR-ENTRY.
           MOVE FUNCTION CURRENT-DATE TO WS-CURRENT-DATE-FIELDS.
           PERFORM PR-SET-DATE-FORMAT
           THRU PR-OTHER.
           GO TO PR-END.

       PR-ERR.
           DISPLAY "ERROR".
           GO TO PR-END.

       PR-DISPLAY-WW.
           MOVE WS-DD OF WS-DATE-WW TO WS-DISPLAY-DATE (1:2).
           MOVE WS-MM OF WS-DATE-WW TO WS-DISPLAY-DATE (4:2).
           MOVE WS-YYYY OF WS-DATE-WW TO WS-DISPLAY-DATE (7:4).
           DISPLAY WS-DISPLAY-DATE.

       PR-DISPLAY-US.
           MOVE WS-MM OF WS-DATE-US TO WS-DISPLAY-DATE (1:2).
           MOVE WS-DD OF WS-DATE-US TO WS-DISPLAY-DATE (4:2).
           MOVE WS-YYYY OF WS-DATE-US TO WS-DISPLAY-DATE (7:4).
           DISPLAY WS-DISPLAY-DATE.

       PR-SET-DATE.
           EVALUATE TRUE
               WHEN WS-US
                   MOVE WS-ACCEPT TO WS-DATE-US
                   MOVE WS-YYYY OF WS-DATE-US TO WS-YEAR
               WHEN WS-WW
                   MOVE WS-ACCEPT TO WS-DATE-WW
                   MOVE WS-YYYY OF WS-DATE-WW TO WS-YEAR
               WHEN OTHER
                   GO TO PR-ERR
           END-EVALUATE.

       PR-DISPLAY.
           EVALUATE TRUE
               WHEN WS-US
                   DISPLAY "You are using the American format, here is t
      -"he date:"
                   PERFORM PR-DISPLAY-US
                   MOVE CORRESPONDING WS-DATE-US TO WS-DATE-WW
                   DISPLAY "The date in the other fromat:"
                   PERFORM PR-DISPLAY-WW
               WHEN WS-WW
                   DISPLAY "You are using the right format, here is the
      -"date:"
                   PERFORM PR-DISPLAY-WW
                   MOVE CORRESPONDING WS-DATE-WW TO WS-DATE-US
                   DISPLAY "The date in the other fromat:"
                   PERFORM PR-DISPLAY-Us
               WHEN OTHER
                   GO TO PR-ERR
           END-EVALUATE.

       PR-SET-DATE-FORMAT.
           DISPLAY "SET THE DATE FORMAT (US/WW)".
           ACCEPT WS-DATE-FORMAT.
      *    SET WS-US TO TRUE.
      *    DISPLAY WS-DATE-FORMAT.

       PR-ACCEPT.
           DISPLAY "SET A DATE.".
           ACCEPT WS-ACCEPT.

       PR-OTHER.
           MOVE WS-ACCEPT TO WS-DISPLAY-DATE.
           PERFORM PR-SET-DATE THRU PR-DISPLAY.
           IF WS-US MOVE CORRESPONDING WS-DATE-US TO WS-DATE-WW
           ELSE IF WS-WW MOVE CORRESPONDING WS-DATE-WW TO
               WS-DATE-US.
      *    PERFORM PR-DISPLAY-WW THRU PR-DISPLAY-US.
           COMPUTE WS-AGE = WS-CURRENT-YEAR - WS-YEAR.
           DISPLAY "Your age (based on your birth year): " WS-AGE.
           MOVE WS-DISPLAY-DATE TO WS-DISPLAY-DATE-ALT.
           DISPLAY WS-DISPLAY-DATE-ALT.

       PR-END.
           STOP RUN.
